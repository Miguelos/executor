<?php

$root = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR;
$dir = implode(DIRECTORY_SEPARATOR, array('test', 'fool', 'executor', 'framework', 'executable')) . DIRECTORY_SEPARATOR;
require_once($root . $dir . 'EchoProgram.php');
require_once($root . $dir . 'CapitalizeProgram.php');
use \fool\test\executor\framework\executable\CapitalizeProgram;

$capitalizer = new CapitalizeProgram($_SERVER['argv']);
$capitalizer->run();
