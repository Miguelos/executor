<?php


namespace fool\test\executor\functional;
use \fool\executor\Passthru;
use fool\test\executor\framework\FunctionalTestCase;

class PassthruTest extends FunctionalTestCase
{
    public function testEchoHelloWorld()
    {
        $passthru = new Passthru();

        $passthru->setProgram('php');
        $passthru->addArgument($this->echo);
        $passthru->addArgument('hello');
        $passthru->addArgument('world');

        $result = $passthru->execute();

        $this->assertEquals(0, $result);
        $this->assertEquals(0, $passthru->getExitStatus());
    }
} 