<?php


namespace fool\test\executor\functional;


use fool\executor\ShellExec;
use fool\test\executor\framework\FunctionalTestCase;

class ExecutorTest extends FunctionalTestCase
{
    public function dataProviderForTestCommandInjection()
    {
        return array(
            array(array('; php', $this->printer)),
            array(array('"; php', $this->printer)),
            array(array('\'; php', $this->printer)),
            array(array('\' && php', $this->printer)),
            array(array('" && php', $this->printer)),
            array(array('&& php', $this->printer)),
            array(array("&& {$this->printer}")),
            array(array('|', 'php', $this->printer)),
            array(array('||', 'php', $this->printer)),
            array(array('&', 'php', $this->printer)),
            array(array(';fork', 'php', $this->printer)),
            array(array('\\', 'php', $this->printer)),
            array(array(':;:', 'php', $this->printer)),
        );
    }

    /**
     * Each test runs the silent program, but the test cases all try to command inject to run the
     * printer program. If anything appears in the output it is considered a successful injection.
     *
     * @dataProvider dataProviderForTestCommandInjection
     */
    public function testCommandInjection(array $arguments)
    {
        $executor = new ShellExec();
        $executor->setProgram('php');
        array_unshift($arguments, $this->silent);
        $executor->setArguments($arguments);

        $output = $executor->execute();

        if (strlen(trim($output)) > 0) {
            $this->fail("A command was allowed to get through");
        }
    }
} 