<?php


namespace fool\test\executor\functional;
use \DirectoryIterator;
use \fool\executor\Exec;
use \fool\test\executor\framework\FunctionalTestCase;

class ExampleTest extends FunctionalTestCase
{
    /**
     * They wouldn't be very good examples if they didnt work
     */
    public function testExampleCodeWorks()
    {
        $directory = "example";
        $iterator = new DirectoryIterator($directory);
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile() && $fileinfo->getExtension() === 'php') {
                $filename = $fileinfo->getFilename();
                $file = $directory . DIRECTORY_SEPARATOR . $filename;
                $exec = new Exec('php', array($file));
                $exec->execute();
                $this->assertTrue($exec->wasSuccessful(), "Example file doesn't execute: $filename");
            }
        }
    }
}
