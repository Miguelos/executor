<?php


namespace fool\test\executor\functional;
use \fool\executor\Exec;
use fool\test\executor\framework\FunctionalTestCase;

class ExecTest extends FunctionalTestCase
{
    public function testEchoHelloWorld()
    {
        $exec = new Exec();

        $exec->setProgram('php');
        $exec->addArgument($this->echo);
        $exec->addArgument('hello');
        $exec->addArgument('world');

        $result = $exec->execute();
        $expectedResult = 'hello world';
        $output = array(
            'hello world'
        );

        $this->assertEquals(0, $exec->getExitStatus());
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($output, $exec->getOutput());
    }
} 