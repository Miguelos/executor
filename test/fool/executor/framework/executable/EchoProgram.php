<?php


namespace fool\test\executor\framework\executable;

/**
 * Echos stuff
 */
class EchoProgram
{
    const MODE_READ_FROM_STANDARD_IN = 1;
    const MODE_READ_FROM_ARGUMENTS = 2;

    /**
     * The command line arguments
     * @var string[]
     */
    private $argv;

    /**
     * @var int
     */
    private $mode;

    /**
     * @param string[] $argv
     */
    public function __construct(array $argv)
    {
        $this->argv = $argv;
        if (count($argv) === 1) {
            $this->mode = self::MODE_READ_FROM_STANDARD_IN;
        } else {
            $this->mode = self::MODE_READ_FROM_ARGUMENTS;
        }
    }

    public function run()
    {
        if ($this->mode === self::MODE_READ_FROM_ARGUMENTS) {
            $this->echoArguments();
        } elseif ($this->mode === self::MODE_READ_FROM_STANDARD_IN) {
            $this->echoStandardIn();
        }
    }

    protected function echoArguments()
    {
        $printableArguments = $this->argv;
        array_shift($printableArguments);
        $this->echoMessage(implode(' ', $printableArguments) . PHP_EOL);
    }

    protected function echoStandardIn()
    {
        if (!feof(STDIN)) {
            while (FALSE !== ($line = fgets(STDIN))) {
                $this->echoMessage($line);
            }
        }
    }

    /**
     * @param string $message
     */
    protected function echoMessage($message)
    {
        echo $message;
    }
}
