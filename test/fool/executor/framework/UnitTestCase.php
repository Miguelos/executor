<?php


namespace fool\test\executor\framework;
use \Exception;
use \ReflectionClass;

class UnitTestCase extends ExecutorTestCase
{
    /**
     * @param string   $exceptionClassName
     * @param callable $fn
     */
    protected function assertWillThrowException($exceptionClassName, callable $fn)
    {
        $exception = null;
        try {
            $fn();
        } catch (Exception $e) {
            $exception = $e;
        }

        if ($exception) {
            $this->assertEquals($exceptionClassName, get_class($exception), "Invalid exception class");
        } else {
            $this->fail("No exception was thrown");
        }

    }

    /**
     * Set a protected property on an object
     *
     * @param  mixed   $object         Any class instance
     * @param  string  $propertyName   The name of a method on $object
     * @param  mixed   $value          The value to set
     */
    protected function setProtectedProperty($object, $propertyName, $value)
    {
        $reflection = new ReflectionClass(get_class($object));
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);

        $property->setValue($object, $value);
    }
} 