<?php

namespace fool\test\executor\unit;

use fool\test\executor\fake\FakeExecutor;
use fool\test\executor\framework\UnitTestCase;

class ExecutorTest extends UnitTestCase
{
    public function dataProviderForTestAddingArguments()
    {
        return array(
            array(array('a', 'b')),
            array(array('--file', 'notes.txt', '-c', '--limit', '50')),
            array(array('aa', '1', '3')),
        );
    }

    /**
     * @dataProvider dataProviderForTestAddingArguments
     */
    public function testAddingArguments(array $arguments)
    {
        $executor = new FakeExecutor();
        foreach ($arguments as $argument) {
            $executor->addArgument($argument);
        }

        $result = $executor->getArguments();
        $this->assertEquals($arguments, $result, "Adding arguments to executor failed");
    }

    public function testBuildCommandSetsCommand()
    {
        $executor = new FakeExecutor();
        $executor->setProgram('echo');
        $executor->addArgument('hello world');

        $executor->doBuildCommand();

        $command = $executor->getCommand();
        $found = preg_match('/echo.*hello.*world/', $command) === 1;
        $this->assertTrue($found, "Executor::buildCommand() did not set \$this->command");
    }

    public function testBuildCommandWhenNoArguments()
    {

        $executor = new FakeExecutor();
        $executor->setProgram('echo');
        $result = $executor->doBuildCommand();
        $hasEcho = strpos($result, 'echo') !== false;
        $this->assertTrue($hasEcho, "Executor::buildCommand() did not work properly with no arguments");
    }

    public function testBuildCommandWhenNoCommand()
    {
        $executor = new FakeExecutor();
        $result = $executor->doBuildCommand();
        $this->assertEquals('', $result, "Executor::buildCommand() did not work properly with no program set");
    }

    public function testConstructorSetsProgramAndArguments()
    {
        $program = 'echo';
        $arguments = array('hello', 'world');
        $executor = new FakeExecutor($program, $arguments);
        $builtCommand = $executor->doBuildCommand();
        $found = preg_match('/echo.*hello.*world/', $builtCommand) === 1;
        $this->assertTrue($found, "An executor created with constructor arguments should add them to the command");
    }

    public function testConstructorArgumentsOptional()
    {
        $executor = new FakeExecutor();
        $builtCommand = $executor->doBuildCommand();
        $this->assertEquals('', $builtCommand, "An executor created with empty constructor should not have any program or arguments");
    }
}