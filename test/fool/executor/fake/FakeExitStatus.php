<?php

namespace fool\test\executor\fake;

use fool\executor\ExitStatus;

/**
 * Use to test ExitStatus code since it is a trait and can't be instantiated directly
 */
class FakeExitStatus
{
    use ExitStatus;

    public function __construct($exitStatus)
    {
        $this->exitStatus = $exitStatus;
    }
}
