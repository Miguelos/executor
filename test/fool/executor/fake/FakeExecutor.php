<?php

namespace fool\test\executor\fake;

use fool\executor\Executor;

/**
 * A concrete subclass of Executor used to test the methods declared in Executor. Since it is
 * abstract you can't instantiate it directly.
 */
class FakeExecutor extends Executor
{
    protected function executeCommand($command)
    {
        /* doesn't do anything */
    }

    /**
     * Helper to aid in testing
     *
     * @return string
     */
    public function doBuildCommand()
    {
        return $this->buildCommand();
    }
}
