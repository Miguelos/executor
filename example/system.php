<?php
$ds = DIRECTORY_SEPARATOR;
$root = dirname(dirname(__FILE__)) . $ds;
$bootstrapFile = "{$root}test{$ds}fool{$ds}executor{$ds}framework{$ds}bootstrap.php";
require $bootstrapFile;
use \fool\executor\System;


/**
 * Using system() to execute:
 *
 * php bin/echo.php hello world
 * > hello world
 */
$system = new System();
$system->setProgram('php');
$system->addArgument("{$root}bin{$ds}echo.php");
$system->addArgument('hello');
$system->addArgument('world');

$system->execute();
