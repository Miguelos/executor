<?php
$ds = DIRECTORY_SEPARATOR;
$root = dirname(dirname(__FILE__)) . $ds;
$bootstrapFile = "{$root}test{$ds}fool{$ds}executor{$ds}framework{$ds}bootstrap.php";
require $bootstrapFile;
use fool\executor\ShellExec;

/**
 * Using shell_exec() to execute:
 *
 * php bin/echo.php hello world
 * > hello world
 */
$shellExec = new ShellExec();
$shellExec->setProgram('php');
$shellExec->addArgument("{$root}bin{$ds}echo.php");
$shellExec->addArgument('hello');
$shellExec->addArgument('world');

$shellExec->execute();
echo $shellExec->getOutput();
