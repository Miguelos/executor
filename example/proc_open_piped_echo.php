<?php

/**
 * This demonstrates how to pipe processes together using ProcOpen.
 */

$ds = DIRECTORY_SEPARATOR;
$root = dirname(dirname(__FILE__)) . $ds;
$bootstrapFile = "{$root}test{$ds}fool{$ds}executor{$ds}framework{$ds}bootstrap.php";
require $bootstrapFile;
use \fool\executor\ProcOpen;

/**
 * Create 2 ProcOpen instances both using the echo.php program.
 */
$echo1 = new ProcOpen();
$echo1->setProgram('php');
$echo1->addArgument("{$root}bin{$ds}echo.php");

$echo2 = new ProcOpen();
$echo2->setProgram('php');
$echo2->addArgument("{$root}bin{$ds}echo.php");

/**
 * Pipe the first echo to the second. When you call execute() on a piped process
 * always call it on the first pipe. It will call execute() on all attatched
 * pipes.
 */
$echo1->pipeTo($echo2);
$echo1->execute();

/**
 * Now that the program has started, write something to standard in
 */
$standardIn = $echo1->getStandardIn();
fwrite($standardIn, 'hello world');
fclose($standardIn);
$echo1->close();

/**
 * Read the output from echo2's standard out
 */
$standardOut = $echo2->getStandardOut();
$result = stream_get_contents($standardOut);
$echo2->close();

echo $result,PHP_EOL;
