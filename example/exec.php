<?php
$ds = DIRECTORY_SEPARATOR;
$root = dirname(dirname(__FILE__)) . $ds;
$bootstrapFile = "{$root}test{$ds}fool{$ds}executor{$ds}framework{$ds}bootstrap.php";
require $bootstrapFile;
use fool\executor\Exec;

/**
 * Using exec() to execute:
 *
 * php bin/echo.php hello world
 * > hello world
 */
$exec = new Exec();
$exec->setProgram('php');
$exec->addArgument("{$root}bin{$ds}echo.php");
$exec->addArgument('hello');
$exec->addArgument('world');

echo $exec->execute(), PHP_EOL;
