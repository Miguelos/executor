<?php


namespace fool\executor;

/**
 * Wrapper for PHP's exec().
 */
class Exec extends Executor
{
    use ExitStatus;

    /**
     * A trimmed array of lines in the output of the command
     *
     * @var string[]
     */
    private $output;

    /**
     * @param  string $command
     * @return string The last line of output
     */
    protected function executeCommand($command)
    {
        $output = array();
        $exitStatus = 0;
        $result = exec($command, $output, $exitStatus);
        $this->exitStatus = $exitStatus;
        $this->output = $output;
        return $result;
    }

    /**
     * All the output of the command as an array of lines
     *
     * @return string[]
     */
    public function getOutput()
    {
        return $this->output;
    }
}
