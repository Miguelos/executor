<?php

namespace fool\executor;

/**
 * Represents a program's exit status. Only available in the execution functions that give an indicator of success.
 */
trait ExitStatus
{
    /**
     * @var int
     */
    protected $exitStatus;

    /**
     * @return bool
     */
    public function wasSuccessful()
    {
        return $this->exitStatus === 0;
    }

    /**
     * @return int
     */
    public function getExitStatus()
    {
        return $this->exitStatus;
    }
}
