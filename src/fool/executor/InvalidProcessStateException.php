<?php

namespace fool\executor;
use \Exception;

/**
 * Thrown when a request is made but the process is in an invalid state
 */
class InvalidProcessStateException extends Exception
{
    /**
     * @param string    $message
     * @param string[]  $errors
     */
    public function __construct($message, array $errors)
    {
        parent::__construct($message . '[' . implode(', ', $errors) . ']');
    }
} 