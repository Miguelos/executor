<?php


namespace fool\executor;


class Passthru extends Executor
{
    use ExitStatus;

    /**
     * @param  string $command
     * @return int The program's exit status
     */
    protected function executeCommand($command)
    {
        $exitStatus = 0;
        passthru($command, $exitStatus);
        $this->exitStatus = $exitStatus;
        return $exitStatus;
    }
}
