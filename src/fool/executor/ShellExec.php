<?php


namespace fool\executor;


class ShellExec extends Executor
{
    /**
     * @var string
     */
    private $output;

    /**
     * Executes the command and returns the output as a string
     *
     * @param  string $command
     * @return string
     */
    protected function executeCommand($command)
    {
        $output = shell_exec($command);
        $this->output = $output;
        return $output;
    }

    /**
     * All the output of the command as a string
     *
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }


} 