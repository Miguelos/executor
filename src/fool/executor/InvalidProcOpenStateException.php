<?php


namespace fool\executor;
use \Exception;

/**
 * Thrown when an action would be invalid in ProcOpen.
 * ProcOpen is only allowed to have execute() called once and this state
 * is guarded by these exceptions. Pretty much all the basic setters have it
 * and execute(). There are many more state changing functions that would be
 * bad to call at certain times that are unguarded. It's only the ones that
 * are likely to be called by users and may lead to unexpected results.
 *
 * Example:
 * - Calling addArgument() after execute()
 * - Changing program name after execute()
 * - Calling execute() more than once
 */
class InvalidProcOpenStateException extends Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
} 